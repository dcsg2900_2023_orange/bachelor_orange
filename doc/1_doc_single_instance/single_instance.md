# How to manually configure and run Moodle using the LAMP method.

Author: Alexander Damhaug, Date: 04.02.2023

Following document is a thorough documentation on how to sucessfully run the Moodle Service on a Ubuntu Server 22.04 LTS (Jammy Jellyfish amd64) Virtual Machine. It is a simplification of Linode's solution, and includes both the installation of a LAMP Stack (Virtual Machine with: Linux, Apache, MySQL, and PhP) And a manual on how to sucessfully run Moodle on the stack.

NOTE: This is the simplest form of running a Moodle platform, and it's purpose is to build a learning curve on understanding the service, and should not be used as a solution itself.

The Sources of this documentation:

* Install a LAMP Stack on Ubuntu 22.04: https://www.linode.com/docs/guides/how-to-install-a-lamp-stack-on-ubuntu-22-04/
* Install Moodle on Ubuntu 22.04: https://www.linode.com/docs/guides/how-to-install-moodle-on-ubuntu-22-04/

## Prerequisite

Following steps is expected to be preconfigured:
- Virtual machine with Ubuntu 22.04 image (manager)
- Terraform installed
- On-prem cloud solution with OpenStack as opensource IaaS tool
- Existing virtual network, where manager is a part of 

See: ```1_doc_openstack_setup.md``` in `5_doc_other_components`

## Setting up the environment

1. Create a directory and move into the empty environment
```
mkdir moodle_Infra
cd moodle_Infra
```

2. Clone the repo with the terraform code: Use own access token below with minimum read-repo rights
```
git clone https://private-token:<access-token>@<link-to-gitlab>
```

3. Copy the directory with the terraform code used and navigate into the dir
```
cp -r bachelor_orange/code/1_code_single_instance/ .
cd 1_code_single_instance/
```

4. Modify the following variables in the terraform file on main.tf to fit your environment in OpenStack
	* ```keypair```
	* ```network ```
	* ```security_groups```

## Run the terraform code and configure the remaining steps

```
terraform init
terraform plan
terraform apply
```


## How to install LAMP (Linux, Apache, MySQL, PhP)

1. Update the ubuntu packages using ```apt```
```
sudo apt update && sudo apt upgrade
```

2. Install apache server using ```apt```
```
sudo apt install apache2
```

3. Install MySQL web server using  ```apt```
```
sudo apt install mysql-server
```

4. Install PHP, along with php modules compatibale with Apache and MySQL using  ```apt```
```
sudo apt install php libapache2-mod-php php-mysql
```

## How to install Moodle on Ubuntu 22.04

1. Ensure Ubuntu system is up to date
```
sudo apt update && sudo apt upgrade
```

2. Confirm release of PHP
```
php -v
```

3. Install the remaining php packages using ```apt```
```
sudo apt-get install graphviz aspell ghostscript clamav php-pspell php-curl php-gd php-intl php-mysql php-xml php-xmlrpc php-ldap php-zip php-soap php-mbstring git
```

4. Remove ; and change setting max_input_vars to atleast 5000 and restart the web server
```
sudo nano /etc/php/8.1/apache2/php.ini
sudo service apache2 restart
```

5. Move to ```/opt``` directory, and clone the Moodle Git repo
```
cd /opt
sudo git clone git://git.moodle.org/moodle.git
```

6. Change to the Moodle directory and list the branches in the Moodle repository. Review the list choose the latest stable release
```
cd moodle
sudo git branch -a
```

7. Track, and choose the desired git-branch
```
sudo git branch --track MOODLE_401_STABLE origin/MOODLE_401_STABLE
sudo git checkout MOODLE_401_STABLE
```

8. Copy the contents of the Moodle repo to the running Apache service, and change the moderation of the moodle directory
```
sudo cp -R /opt/moodle /var/www/html/
sudo chmod -R 0777 /var/www/html/moodle
```
NOTE: For security reasons, consider changing the directory permissions

9. Create the ```/var/www/moodledata``` directory and change owner and permissions
```
sudo mkdir /var/www/moodledata
sudo chown -R www-data /var/www/moodledata
sudo chmod -R 0777 /var/www/moodledata
``` 

## How to configure the MySQL Server for Moodle

1. Create a password generated for the admin user
```bash
sudo apt install pwgen

pw=$(pwgen -s 24 1)
sudo bash -c "echo $pw >> password.txt"
pw=""
sudo chmod 600 password.txt
```

2. Log into mySQL using ```root``` privileges and create a password
```
sudo mysql -u root -p 
```

3. Create a database for Moodle
```
CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

4. Create a Moodle MySQL user, and grant them permissions for the database.
```
CREATE USER 'admin'@'localhost' IDENTIFIED BY '$(cat passord.txt)';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'admin'@'localhost';
```

5. Exit database
```
quit
```

## How to configure the Moodle service to finish installation GUI

1. Go in the browser and put the following in the URL: ```<floating_IP>/moodle```

2. Finish the installation, by following the steps provided in the browser
```
Choose Language -> Next -> Confirm paths -> Choose database driver: Improved MySQL database driver -> Database settings:

Database host:     Localhost
Database name:     Moodle
Database user:     admin
Database password: <generated-password>

Review the licensing agreement and continue
```

3. Moodle verifies installations, Ensure all ```server checks``` indicate ```ok```. If the installation is successful, Moodle displays the message ```Your server environment meets all minimum requirements``` select **Continue**

4. Moodle will now install every service it provides in the background, this may take some time, so just be patient.

5. Press **continue** and configure the main administrator account which will have complete control over the platform.

```
Username:      <username>
Password:      <Password>
First Name:    <first_name>
Last Name:     <last_name>
Email Address: <Email_Address>
City/Town:     <City/Town>
Country:       <Country>
Timezone:      <Timezone>
Description:   <Description>
```

1. Installation of home web page

```
Full site name: <site_name>
Short name for site: <short_name>
Support Email: <support_email>
Noreply address: <noreply_address>
```

## Now you have a fully working Moodle platform!
