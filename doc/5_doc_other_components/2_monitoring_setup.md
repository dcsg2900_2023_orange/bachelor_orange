# Set up monitoring of services (linux monitoring): 

@ Author Kristoffer Lie



First, we need to provision a influxdb server, witch is done using terraform.

Get the code: 

git clone https://private-token:<access-token>@<link-to-gitlab>

Extract the folder needed and move into it: 
```
cp -r code/5_code_other_components/monitoring/ .

cd monitoring. 
```


Some of the variables in variables.tf should be changed: 
- `instance_key_pair`, 
- `network1` 
- `network_pool`
- `security_groups`
- `instance_flavor`

```
nano variables.tf
```


Running `terraform init` followed by `terraform apply` will provision a instance with influxdb installed and started, together with a floating IP to access the servcer. 

Head to the server floating ip and port 8086 to get started: 

	http://<floating_ip>:8086

Log in with the cridentials provided in the startupscript. 


## Add a template dashboard: 

Head to the dashboard tab on the left --> create dashboard --> add template --> browse comunity templates. 

Choose the template you prefer, we use the linux system template: 

	https://github.com/influxdata/community-templates/tree/master/linux_system

Copy the .yaml url into the influxdb site and click `Lookup Template` --> install template

If you head back to your dashboard you should now see a linux system dashboard. 



Generate a API token to be used to connect to the influxDB in the API token tab. Take note of the Token generated and store it somewhere safe!

## Setup telegraf to run (on evry host)

Note: inspierd by this site: https://serverfault.com/questions/1057001/telegraf-works-manually-but-not-the-service-run-telegraf-in-background

Install telegraf: 

	sudo apt install telegraf

Verify that telegraf is installed: 

	telegraf version

Curl down the config files: 

	 curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>"/bachelor_orange/config/env_telegraf >> env_telegraf

	 curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>"/bachelor_orange/config/linux_montr.conf >> /var/www/html/moodle/linux_montr.conf

Fill out the env variables in the env_telegraf file to fit your influx setup and send it to telegraf  

	nano env_telegraf 
	
	sudo cp env_telegraf /etc/default/telegraf

Edit the hostname to match the instance. 

	nano linux_montr.conf

	``

Copy the telegraf configuration file to let telegraf service read from it: 



	sudo cp linux_montr.conf /etc/telegraf/telegraf.conf


Restart the telegraf service and check the status

	sudo systemctl restart telegraf
	
	sudo systemctl status telegraf

After 5 minutes, head to the linux system dashboard! Spesify the bucket and host. 