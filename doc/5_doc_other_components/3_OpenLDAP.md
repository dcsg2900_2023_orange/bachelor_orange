# OpenLDAP

- [OpenLDAP](#openldap)
  - [Config/installation](#configinstallation)
  - [Create users](#create-users)
  - [Setup Moodle to use LDAP server](#setup-moodle-to-use-ldap-server)
  - [Configure cron job](#configure-cron-job)

## Config/installation

Installation:

```bash
sudo apt install slapd ldap-utils -y

sudo dpkg-reconfigure slapd
#  - Omit OpenLDAP server configuration? No
#  - DNS domain name? orange.ba
#  - Organization name? Bachelor Orange
#  - Administrator password?
#  - Do you want the database to be removed when slapd is purged? No
#  - Move old database? Yes
```

## Create users

```bash
# Setting variable for generated passwords for user
sudo apt install pwgen

password=$(pwgen -s 24 1)
sudo bash -c "echo $password >> password.txt"
sudo chmod 600 password.txt

# Create OU
sudo ldapadd -x -D cn=admin,dc=orange,dc=ba -W << EOF
dn: ou=People,dc=orange,dc=ba
objectClass: organizationalUnit
ou: People
EOF

# Create users
sudo ldapadd -x -D cn=admin,dc=orange,dc=ba -W << EOF
dn: uid=User,ou=People,dc=orange,dc=ba
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
uid: User
givenName: User
sn: User
cn: User User
displayName: User User
mail: user@user.user
uidNumber: 1001
gidNumber: 1001
userPassword: $password
gecos: User User
loginShell: /bin/bash
homeDirectory: /home/user

dn: uid=User2,ou=People,dc=orange,dc=ba
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
uid: User2
givenName: User2
sn: User
cn: User2 User
displayName: User2 User
mail: user2@user.user
uidNumber: 1002
gidNumber: 1001
userPassword: $password
gecos: User2 User
loginShell: /bin/bash
homeDirectory: /home/user2

dn: uid=User3,ou=People,dc=orange,dc=ba
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
uid: User3
givenName: User3
sn: User
cn: User3 User
displayName: User3 User
mail: user3@user.user
uidNumber: 1003
gidNumber: 1001
userPassword: $password
gecos: User3 User
loginShell: /bin/bash
homeDirectory: /home/user3

dn: uid=User4,ou=People,dc=orange,dc=ba
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
uid: user4
givenName: User4
sn: User
cn: User4 User
displayName: User4 User
mail: user4@user.user
uidNumber: 1004
gidNumber: 1001
userPassword: $password
gecos: User4 User
loginShell: /bin/bash
homeDirectory: /home/user4
EOF

# Clearing password variable
password=""
```

This creates the following structure:

- orange.ba
  - People
    - User
    - User2
    - User3
    - User4

User passwords are set using a saved password file. The file with the password is only accessible as root, and the file is deleted as soon as it's not needed anymore.

## Setup Moodle to use LDAP server

<https://docs.moodle.org/401/en/LDAP_authentication#Enabling_LDAP_authentication>

1. Go to Site administration -> Plugins -> Authentication -> Manage authentication and click the eye icon opposite LDAP Server. When enabled, it will no longer be greyed out.
2. Click the settings link, configure as required (see information below), then click the 'Save changes' button.

Configure the following settings:

| Setting                              | New value                 |
| -------                              | ---------                 |
| auth_ldap &#124; host_url            | ldap://10.212.174.170     |
| auth_ldap &#124; preventpassindb     | Yes                       |
| auth_ldap &#124; bind_dn             | cn=admin,dc=orange,dc=ba  |
| auth_ldap &#124; bind_pw             | YOUR LDAP ADMIN PASSWORD  |
| auth_ldap &#124; user_type           | posixAccount (rfc2307)    |
| auth_ldap &#124; contexts            | ou=people,dc=orange,dc=ba |
| auth_ldap &#124; field_map_firstname | givenName                 |
| auth_ldap &#124; field_map_lastname  | sn                        |
| auth_ldap &#124; field_map_email     | mail                      |

## Configure cron job

<https://docs.moodle.org/401/en/LDAP_authentication#Enabling_the_LDAP_users_sync_job>

1. Go to Site administration -> Server -> Scheduled tasks and click the gear icon on LDAP users sync job.
2. Select the desired frequency of running and enable the task by un-ticking the disabled checkbox.

Run the following command on the Moodle server to check if the cron jobs work:

```bash
sudo /usr/bin/php /var/www/html/moodle/admin/cli/cron.php
```

Next configure cron on the server to run the previous command automatically:

<https://docs.moodle.org/36/en/Cron_with_Unix_or_Linux>

```bash
sudo crontab -u www-data -e

# Add this line:
*/1 * * * * /usr/bin/php /var/www/html/moodle/admin/cli/cron.php >/dev/null
```
