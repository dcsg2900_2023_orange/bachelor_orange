# How to create a stack of components in Openstack.

Author: Alexander Damhaug, Date: 30.01.2023

Following document is a thorough documentation on how to successfully build a stack of components in GUI for Openstack.

## Create a Router

* A router is a virtual component that connects multiple networks within the Openstack environment. This allows the user to connect their virtual network with the gateway of the on-prem solution.
* 'ntnu-internal' refers to the standarized internal network in SkyHiGH (NTNU's on-prem cloud platform that runs the Openstack software). An external user, typically has a similar option, with a different name.

```
Network     -> Routers        -> Create Router -> Router Name: "default" -> create router -> 
Set Gateway -> Select network -> ntnu-internal -> submit
```

## Create a Network

* In this step, you have to create your own private network, an environment your instances can launch on. 

```
Network -> Networks        -> Create Network -> Network Name: "default" -> Subnet -> 
Subnet Name: "default"     -> Network Address: 192.168.X.X/24 -> Create
```

## Create Security Groups

* A security group is essentially a simple firewall, to block or allow specific types of traffic or ip-addresses.

```
Security Group -> Name: "default"  -> Create Security Group -> 
Add rules      -> Rule: HTTP, HTTPS, SSH -> Ingress -> Add
```
NOTE: Ensure that all ip protocols and any port range is open for traffic within the network
<!-- Should limit Ip-addresses that can accsess the service-->

## Create key pair / Import public key

* Creating a public key for your openstack project is necessary to be able connect to instances created.

```
Compute -> Key Pairs -> Import Public Key -> Key Pair Name: "default" 
                     -> Key Type: SSH Key -> Browse: default.pub -> Import Public Key
```
<!-- ONLY USE .PUB!! KEYS -->

## Create Main Instance (Manager)

* Use the latest available image.
* Following section is an example of compute resources, that can be applied.

```
Instances -> Launch Instance -> 
Instance Name: "Test_Manager"
Source: 'Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64'
Flavor: 'gx1.2c4r' (2 VCPUS, 4GB, 40 GB Total Disk) 
Networks: "default" 
Security Groups: "default" -> Security Groups -> Key Pair -> default -> Launch Instance
```

## Associate Floating IP

* Associate a floating IP to the instance created, to reach it outside the network.

```
Instances -> "Test_Manager" -> Dropdown    -> Associate Floating IP -> + 
                            -> Allocate IP -> Select a port: Test_Manager -> Associate
```

## SSH into the VM

* Open a secure connection the instances created.

```
ssh -i .\default ubuntu@<floating_IP>
```

## What's next?

- Run Moodle: See Documentations -> doc/1_doc_single-instance/single_instance.md

NOTE: You can start at "How to install LAMP (Linux, Apache, MySQL, PhP)" if you've finished `1_doc_openstack_setup.md`