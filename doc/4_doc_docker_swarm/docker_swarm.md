# This document goes through the setup and configuration of docker swarm running Moodle. 

Author: Kristoffer Lie, Date: 08.05.2023

## Short description: 

The docker swarm architecture is a combination of all our previous infrastructures and knowledge. Docker swarm is a container orchestration tool that allows us to run up redundant amount of services that all runs the same docker image. Docker itself handles what container runs where and load balancing internal. 

Following steps in the documentation:
1. 3 ubuntu instances
2. Installed and setup MariaDB cluster 
3. Set up shared network folder using glusterFS
4. Install and setup docker, including docker swarm
5. Run database container (install DB)
6. Set up openstack loadbalancer (web and db)
7. Deploy the stack 

## Prerequisite:

Following steps is expected to be preconfigured:
- Virtual machine with Ubuntu 22.04 image (manager)
- Terraform installed
- On-prem cloud solution with OpenStack as opensource IaaS tool
- Existing virtual network, where manager is a part of 


## Use terraform to set up the three servers: 

The code needed is stored in the folder  ‘/code/4_code_docker_swarm’. This sets up three servers in openstack. Software is installed with a startup script. 

Edit these variables befor you apply the code:`

```
/modules/server/variables.tf

    instance_key_pair
    network
    security_groups

/modules/server/startupscript.sh

    docker registry ip on line (at the end)    
```

Apply the code

```
Terraform init
terraform apply
```
Note that you need terraform installed and connect to the openstack provider. 

Note down the ip addresses for each serves: 

```
server1: <ipadd>
server2: <ipadd>
server3: <ipadd>
```

## Configure the db-servers

### On every DB server
1. Open a secure connection between all the provisioned servers through ssh from main. 
```
ssh <ip-address>
sudo su
```

2. Configure local DNS to map the ip-addresses of the 3 DB-servers in /etc/hosts 'db1-db2-db3' 

serverX: `nano /etc/hosts`:
```
<server1> db1
<server2> db2
<server3> db3
```

3. Ping domain-names to assure connectivity on the network
`ping dbX`

4. Copy the contents of the following DB-cluster configuration file
- Source: - MariaDB cluster setup: 
```
[mysqld]  
# Cluster node configurations  
wsrep_cluster_address="gcomm://db1,db2,db3"  
# Make sure this is corresponds to hostname:  
wsrep_node_address="dbx"  
innodb_buffer_pool_size=600M  

# Mandatory settings to enable Galera  
wsrep_on=ON  
wsrep_provider=/usr/lib/galera/libgalera_smm.so  
binlog_format=ROW  
default-storage-engine=InnoDB  
innodb_autoinc_lock_mode=2  
innodb_doublewrite=1  
query_cache_size=0  
bind-address=0.0.0.0

# Galera synchronization configuration  
wsrep_sst_method=rsync
```

6. Create and modify `cluster.cnf` to the servers specification, so it can join the cluster. Change the following argument "dbx" to db1-3 on the respective server you are on. 
server1: `nano /etc/mysql/conf.d/cluster.cnf`:
```
wsrep_node_address="db1"
```
server2: `nano /etc/mysql/conf.d/cluster.cnf`:
```
wsrep_node_address="db2"
```
server3: `nano /etc/mysql/conf.d/cluster.cnf`:
```
wsrep_node_address="db3"
```

### On one DB-server (server1)
1. Start server1 in bootstrap mode, this server will be the master of this database-cluster: 
`galera_new_cluster`

### On the remaining servers
1. Start their database and confirm connectivity to the cluster. Complete synchronization can take a few minutes.
```
service mysql restart
service mysql status
```
2. Check cluster size
```
mysql -u root -p"" -e "SHOW STATUS LIKE 'wsrep_cluster_size'"
```

### One DB-server: 
1. Create the Moodle database, the admin user with the generated password and restart the DB
```
pw=$(pwgen -s 24 1)
sudo bash -c "echo $pw >> password.txt"
sudo chmod 600 password.txt


sudo mysql -u root -p"" -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"

sudo mysql -u root -p"" -e "CREATE USER 'admin'@'%' IDENTIFIED BY ‘$pw’;" 

sudo mysql -u root -p"" -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'admin'@'%';"

sudo sed -i 's/#bind-address.*/bind-address = 0.0.0.0/' /etc/mysql/mariadb.conf.d/60-galera.cnf

sudo systemctl restart mysql

pw=""

```
2. Connect to another server in the cluster and check if the database is available
```
mysql -u root -p"" -e "SHOW DATABASES"
```


### Set up glusterFS

Probe the the servers from server1

```
gluster peer probe <server2_ip>
gluster peer probe <server3_ip>

```

Create the gluster volume

```
gluster volume create moodle replica 3 <server1>:/moodle_brick <server2>:/moodle_brick <server3>:/moodle_brick force

```

Start the volume: 

```
gluster volume start moodle
```


Mount the volume on each server: 
```
mount -t glusterfs <serverx_ip>:moodle /moodledata
```

Verify connection by creating a file on server1: 

```
touch /moodledata/I_AM_MOUNTED
```

On server2 and server3: 
```
ls /moodledata
```

give the proper access to moodledata: 
```
sudo chown -R www-data /moodledata
sudo chmod -R 0777 /moodledata
```

You should now have a shared network folder mounted on /moodledata


### Create a loadbalancer in GUI for HTTP 

Create a loadbalancer in openstack GUI. 


Update the floating IP of wwwroot in config.php in the gitlab repo. 
Set the dbhost to be server1 private IP. 

### Set up the database: 

First, we need to pull down the image from our local docker registry. 

To allow http pull, add this registry: 


```
echo "{
\"insecure-registries\" : [\"http://<registry_ip>:5000\"]
}" >> /etc/docker/daemon.json

sudo systemctl restart docker
```

Pull and run the install database installation script: 

```
sudo docker pull <registry_ip>:5000/moodle_install_db_script

sudo docker run -P <registro_ip>:5000/moodle_install_db_script:latest
```

### Set up the swarm: 


Initialize the swarm on server1

```
docker swarm init
```

Take note of the join command outputted and paste it in the remaining servers. 


### Get the docker-compose file and start the moodle service! 

Get the docker compose file and haproxy config in the /code/config folder: 
```
curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>/config/docker_compose.yaml" >> docker_compose.yaml

curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>/config/haproxy.cfg" >> haproxy.cfg
```




### Start the moodle service! 

In order for the LB container to work, edit the dbhost to 'getenv("MOODLE_DB_HOST")' before you start up the stack. 

Pull down the web service image: 
```
sudo docker pull <registry_ip>:5000/moodle_www_krisern
```

Run the stack

```
sudo docker stack deploy -c docker_compose.yaml moodle
```

Use the set_premissions_moodledata.sh to set the preferd premissions on dataroot  (/moodledata). 


```
 curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>" >> set_premissions_moodledata.sh
```

make the file executable, and run the set premissions script: 
```
./set_premissions_moodledata.sh /moodledata
```

