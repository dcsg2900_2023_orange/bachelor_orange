# Docker

This document goes through the setup and configuration of docker to set up moodle

## Short description: 

The docker architecture is set up to run multiple containers with the self-made docker image. 

## Prerequisite:

Following steps is expected to be preconfigured:
- Virtual machine with Ubuntu 22.04 image (manager)
- Terraform installed
- On-prem cloud solution with OpenStack as opensource IaaS tool
- Connection to the openstack provider.
- Existing virtual network, where manager is a part of 

See `/doc/5_doc_other_components/1_doc_openstack_setup.md` for manual prosess to set up these prerequisite.

## Use terraform to set the infrastructure. 

Clone down the needed code in the folder ‘/code/3_code_docker/infra_code/’

    git clone https://private-token:<access-token>@<link-to-gitlab>

    cp -r  ./bachelor_orange/3_code_docker/docker_code .


Edit these variables before you apply the code:

```
/modules/<module_name>/variables.tf

    instance_key_pair
    network
    security_groups

/modules/docker/startupscript.sh

    docker registry ip 
```

Modules (database, docker and docker_loadbalancer)

Apply the code



```
Terraform init
terraform apply
```

This code sets up three servers in openstack. These are a docker instance, a database and a loadbalancer for docker containers. Software is installed with startup scripts. 


After a few minutes the three instances are created. Both the database and loadbalancer does the software configuration for you. 

If you have prebuilt images pushed to the private registry, follow this guide [docker_registry.md](docker_registry.md). To build your own images localy, see this guide [docker_build.md](docker_build.md). Continue on the newly created docker instance.

