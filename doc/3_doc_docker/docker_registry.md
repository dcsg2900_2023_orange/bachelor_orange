## Using images from private docker registry

This document guides you trough how to use images stored on a private docker registry server. 

**Before running:**
Edit `dbhost` to database IP and `www-root` to "http://balancer-floating-ip/moodle" in `bachelor_orange/config/config.php` and push changes.
Ensure that the database type is set to `mysqli`


Pull images:
```
sudo docker pull <registry_IP>:5000/moodle_install_db_script
sudo docker pull <registry_IP>:5000/moodle_www
```

### Running the containers:

If the db is new, make sure the moodle database is created and run the install database container:
    
    sudo docker run -P <registry_IP>:5000/moodle_install_db_script:latest

Note that this may take some time to run trough the database initialzation.  

### Start the webserver container:

To start webserver container run 
```
sudo docker run -d -p 22222:80 -v moodledata:/moodledata 10.212.170.44:5000/moodle:latest
sudo docker run -d -p 33333:80 -v moodledata:/moodledata 10.212.170.44:5000/moodle:latest
sudo docker run -d -p 44444:80 -v moodledata:/moodledata 10.212.170.44:5000/moodle:latest
```

Remember to open for ports/portrange used for containers in sec-group. 

You can now visit the running service at http://<balancer-floating-ip>/moodle 
