# Setup to build images on a docker instance:

In home directory:
clone `bachelor_orange` repo
	
    git clone https://private-token:<access-token>@<link-to-gitlab>

  	
Copy the files to your location

    cp -r  ./bachelor_orange/3_code_docker/docker_code .

Go into the folder: 

    cd docker_code/


Clone the moodle code from moodle github: 

    git clone -b MOODLE_401_STABLE git://git.moodle.org/moodle.git


Build the two images needed, moodle_www:

    cd docker_www
    
    sudo docker build -t moodle_www:<version-tag> -f Dockerfile ..

And detabase_script image:

    cd ../docker_moodle_db-setup

    sudo docker build -t moodle_www:<version-tag> -f Dockerfile ..


## How to run

**Before running:**
The database pasword is needed and is found in `/home/ubuntu/password.txt`
Edit `dbhost` to database ip, `www-root` to "http://balancer-floating-ip/moodle" and `dbpass` to the database password in `bachelor_orange/config/config.php` and push changes.
Ensure that the database type is set to `mysqli`

Additionally, to be able to prepare the database for use with Moodle, the database password must be set in `bachelor_orange/code/3_code_docker/docker_code/docker_moodle_db-setup`.


## Running the containers:

If the db is new, make sure the moodle database is created and run the install database container:
    
    sudo docker run -P moodle_install_db_script:latest

Note that this may take some time to run trough the database initialzation.  

### Start the webserver container:

To start webserver container run 
```
sudo docker run -d -p 22222:80 -v moodledata:/moodledata moodle:latest
sudo docker run -d -p 33333:80 -v moodledata:/moodledata moodle_www:latest
sudo docker run -d -p 44444:80 -v moodledata:/moodledata moodle_www:latest
```

Remember to open for ports/portrange used for containers in sec-group. 

You can now visit the running service at http://<balancer-floating-ip>/moodle
