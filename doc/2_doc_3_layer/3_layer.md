# How to configure a complete infrastructure with the moodle-platform using terraform

Author: Alexander Damhaug, Date: 28.02.2023

Following document is a thorough documentation on how to successfully run the Moodle platform on a complete self-provisioned infrastructure with a separate load-balancer, database and an optional amount of web-servers for configuration. In this document, we use the Ubuntu Server 22.04 LTS (Jammy Jellyfish amd64) Virtual Machine for the compute resource.

NOTE: This documentation, uses preconfigured "terraform code and bash script" for provisioning the infrastructure and the Moodle platform itself.

Sources:

* How to implement sticky sessions with haproxy: https://www.haproxy.com/blog/enable-sticky-sessions-in-haproxy/
* How to cluster multiple servers: https://severalnines.com/blog/clustering-moodle-multiple-servers-high-availability-and-scalability/
* ChatGPT - specifically: Bash script-syntax 
* Lecture from DCSG2003, Uke 10: "Nettverkslagring"
* Lecture from DCSG2003, Uke 3: "Arkitekturer og lastbalansering"

## Prerequisite

Following steps is expected to be preconfigured:
- Virtual machine with Ubuntu 22.04 image (manager)
- Terraform installed
- On-prem cloud solution with OpenStack as opensource IaaS tool
- Existing virtual network, where manager is a part of 

See: ```1_doc_openstack_setup.md```

## Setting up the environment

1. Create a directory and move into the empty environment
```
mkdir moodle_Infra
cd moodle_Infra
```

2. Clone the research repo with the terraform code: Use own access token with read-repo rights
```
git clone https://private-token:<access-token>@<link-to-gitlab>
```

3. Copy the directory with the terraform code used and navigate into the dir
```
cp -r bachelor_orange/code/1_code_single_instance/ .
cd 1_code_single_instance/
```

4. Modify the following variables in the terraform files in the module folder to fit your environment in OpenStack
	* ```instance_count    (Amount of webservers)```
	* ```instance_name    ```
	* ```instance_flavor  ```
	* ```instance_key_pair (Manager public key)```
	* ```network           (Same as manager)```
	* ```security_groups  ```
```
database -> variables.tf
loadbalancer -> variables.tf
webservers -> variables.tf
```

5. Make sure to also modify the loadbalancer startup script with the correct repo location
```
loadbalancer -> startupscript.sh
```

## Run the terraform code and configure the remaining steps

```
terraform init
terraform plan
terraform apply
```

NOTE: This configuration can take several minutes, be patient!

### Configure Gluster FS between the **webservers**

NOTE: Replace <"server1-x"> section with the ip-addresses of applicable webserver

#### On every server
1. Open a secure connection between all the provisioned webservers through ssh from main. Change the hostname to web1-x to differentiate the webservers
```
ssh <ip-address>
sudo hostname <webX>
sudo su
```

#### On one server (server1)
1. Run the following command(s), but only use the ip-addresses of the remaining webservers
```
gluster peer probe <server2>
gluster peer probe <server3>
...
gluster peer probe <serverX>
```

NOTE: If 'gluster' not found is the output, the startup installation is not finished yet, wait some minutes and try again. Check the status.txt: `cat ~/status.txt`

2. Check if every webserver is within the same cluster
```
gluster peer status
```
3. Create and run the volume (Change number to amount of webservers provisioned) 
```
gluster volume create moodle replica 2 <server1>:/moodle_brick <server2>:/moodle_brick <server3>:/moodle_brick  force

gluster volume start moodle
```

#### On every server
1. Mount the the volume on the existing moodledata directory: serverX is the ip-address of the server you are running the command on
```
mount -t glusterfs <serverX>:moodle /var/www/moodledata
```

2. Check if the mount was successful
```
df -h
```

#### On one server
1. Change the owner and permissions for the /moodledata directory
```
sudo chown -R www-data /var/www/moodledata
sudo chmod -R 0777 /var/www/moodledata
```

### Configure the config.php, start CLI installation, and finish configuration in GUI
* Go back to manager, and retrieve the config-file in the research-repo 
```
cp /home/ubuntu/moodle_Infra/research/config/config.php .
```
* Modify the `config.php` to your specification, in our solution you only need to change the following arguments: 
```
$CFG->dbhost    = '<DB_ip-address>';
$CFG->wwwroot   = 'http://<LB_floating-IP>/moodle';
```
* Create config.php file under /var/www/html/moodle directory in every webserver and copy the config contents earlier and paste it in the same config.php file
```
ssh <ip-address>
sudo su
nano /var/www/html/moodle/config.php
^U
```
* On Server1: Change the password below to the Admin password generated for Moodle in the startupscript, and run the command to start the installation process: (Can take several minutes to finish installation)
```
php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=<generated_password>
```
* Copy the floating-IP and paste it in the browser
* Log in as the admin you've just created, and finish and update the profile
* Finish the site settings configuration and save changes
## Now you have a fully working Moodle platform on a provisioned infrastructure, with Load-Balancing!
