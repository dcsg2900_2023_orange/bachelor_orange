
# The file contains variable declaration and some default values. 
# This file together with main.tf and terraform.tfvars provision and instantiate a infrastructure stack
# See teraform documentation for more details of syntax/code

# @ File variables.tf
# @ Author Kristoffer Lie, NTNU


variable "network_pool" {
   type = string

   default = "default"

   description = "Public/floating netowrk pool"

}

variable "instance_key_pair" {
 type = string
 default = "default"
 description = "SSH key to be used to connect to the instance"
}

variable "network1" {
  type    = string
  default = "default" # default network to be used
}

variable "security_groups" {
  type    = list(string)
  default = ["default"]  # Name of default security group
}


variable "instance_image" {
 type = string
 default = "Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64"
 description = "Image for the server"
}

variable "instance_flavor" {
 type = string
 default = "gx3.4c4r"
 description = "Flavor for the server"
}

#### Influxdb #### 

# Define variables: 
variable "instance_name" {
 type = string
 default = "influxdb"
 description = "Name of the instance"

}

variable "startup_script" {
   type = string
   default = "./startupscript.sh"
   description = "startup script to be run on the instance"
}





