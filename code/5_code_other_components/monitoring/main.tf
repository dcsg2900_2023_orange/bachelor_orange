# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
# Note that the clouds.yaml from openstack is located inside ``.config/openstack/`` folder
provider "openstack" {
    cloud = "openstack"
}



### influxdb instance ###
resource "openstack_compute_instance_v2" "influxdb" {
  name = var.instance_name
  image_name = var.instance_image
  flavor_name = var.instance_flavor
  key_pair = var.instance_key_pair
  security_groups = var.security_groups
 
  network {
    name = var.network1
  }

  user_data = file(var.startup_script)
}




 # Get a floating IP
 resource "openstack_networking_floatingip_v2" "fip_1" {
   pool = var.network_pool_ntnu
 }


 # Connect Floating IP to instance
 resource "openstack_compute_floatingip_associate_v2" "fip_1" {
   floating_ip = "${openstack_networking_floatingip_v2.fip_1.address}"
   instance_id = "${openstack_compute_instance_v2.influxdb.id}"
 }



#output ip influxdb
output "influxdb_IP" {
  value = openstack_compute_instance_v2.influxdb.network[0].fixed_ip_v4
}
#output ip influxdb
output "influxdb_floating_IP" {
  value = openstack_networking_floatingip_v2.fip_1.address
}


