#!/bin/bash

# @author: Kristofer Lie, date: 19.04.2023
echo "startupscript started" >>~/status.txt

# Update ubuntu packages
sudo apt update && sudo apt upgrade

# use NTP to syncronise the internal server clocks
sudo apt-get install -y ntpdate
sudo ntpdate -b ntp.justervesenet.no

# Use crontab to frequently update the clock to follow NTP to assure accurate syncronisation
echo "*/10 * * * * root ntpdate -b ntp.justervesenet.no" >> /etc/crontab


# Install MariaDB Galera Cluster
sudo apt-get install mariadb-server mariadb-client galera-4 -y
echo "Installed mariadb-cluster galera" >>~/status.txt


# Install gluster FS
apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd
echo "GlusterFS sucsessfully installed and started" >>~/status.txt

# Create folders to be used in gluster: 
mkdir /moodle_brick
mkdir /moodledata

# From docker docks: https://docs.docker.com/engine/install/ubuntu/
# Install docker: 
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
