# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
    cloud = "openstack"
}


# Variables
variable "keypair" {
  type    = string
  default = "default"   # name of keypair created 
}


variable "network" {
  type    = string
  default = "default" # default network to be used
}

variable "security_groups" {
  type    = list(string)
  default = ["default"]  # Name of default security group
}

# Data sources
## Get Image ID
data "openstack_images_image_v2" "image" {
  name        = "Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64" # Name of image to be used
  most_recent = true
}

## Get flavor id
data "openstack_compute_flavor_v2" "flavor" {
  name = "gx3.4c4r" # flavor to be used
}


# Get a floating IP
resource "openstack_networking_floatingip_v2" "fip_1" {
  pool = "ntnu-internal"
}

# Create a terraVM
resource "openstack_compute_instance_v2" "TerraVM_Alex" {
  name            = "single_instance"
  image_id        = data.openstack_images_image_v2.image.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = var.keypair
  security_groups = var.security_groups

  network {
    name = var.network
  }

  user_data = file(var.startup_script)
}

resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_1.address}"
  instance_id = "${openstack_compute_instance_v2.TerraVM_Alex.id}"
}
