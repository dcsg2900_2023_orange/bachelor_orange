#!/bin/bash

# Retrieve the IP-addresses of the webservers
echo ${IP} | tr ',' '\n' >>~/ipaddresses.txt
IP_ADDRESSES=($(cat ~/ipaddresses.txt))

echo "startupscript started" >>~/status.txt

# clone working directory
cd ~/
git clone https://private-token:<access-token>@<link-to-gitlab>

echo "cloned git-repo" >>~/status.txt

# Run the host configuration script
~/bachelor_orange/code/scripts/hosts.sh

# Run the haproxy script
~/bachelor_orange/code/scripts/docker_haproxy.sh


echo "Script sucessfully ran" >>~/status.txt