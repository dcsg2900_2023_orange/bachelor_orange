#!/bin/bash

echo "startupscript started" >>~/status.txt

# Update ubuntu packages
sudo apt update && sudo apt upgrade

# Install docker and dependencies from https://docs.docker.com/engine/install/ubuntu/
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo "Installed docker and dependencies" >>~/status.txt

sudo service docker start

# Allow pull from http docker registry
echo "{
\"insecure-registries\" : [\"http://<registry_ip>:5000\"]
}" >> /etc/docker/daemon.json

sudo service docker restart

# Create the moodledata directory 
mkdir /moodledata
sudo chown -R www-data //moodledata
sudo chmod -R 0777 /moodledata 

# note, the accsess to the moodledata directory and moodle folder should be restricted and follow Moodle's security recomendations:
#     https://docs.moodle.org/402/en/Security_recommendations

echo "startupscript done" >>~/status.txt