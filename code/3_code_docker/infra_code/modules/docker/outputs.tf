# main.tf

output "instance_private_ip" {
  value = openstack_compute_instance_v2.docker.*.access_ip_v4
}