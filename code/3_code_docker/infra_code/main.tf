# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
# Note that the clouds.yaml from openstack is located inside ``.config/openstack/`` folder
provider "openstack" {
    cloud = "openstack"
}
# initialize the database module
module "database" {
  source = "./modules/database/"
}
# initialize the loadbalancer module
module "loadbalancer" {
  source = "./modules/docker_loadbalancer/"
}



