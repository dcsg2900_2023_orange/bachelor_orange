#!/bin/bash -x

# get the php config file from repostory
curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>" >> /var/www/html/moodle/config.php


# Run crontab
/usr/bin/php /var/www/html/moodle/admin/cli/cron.php

echo "*/1 * * * * /usr/bin/php /var/www/html/moodle/admin/cli/cron.php >/dev/null" | crontab -u root -

#/usr/bin/php /var/www/html/moodle/admin/cli/install_database.php
/usr/sbin/apache2ctl -D FOREGROUND -k start
