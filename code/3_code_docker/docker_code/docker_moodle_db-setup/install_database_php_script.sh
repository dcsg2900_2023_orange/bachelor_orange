#!/bin/bash -x

curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api> >> /var/www/html/moodle/config.php"

# Change --adminpass to the password created for the Admin user
/usr/bin/php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass='<your_generated_password>'
