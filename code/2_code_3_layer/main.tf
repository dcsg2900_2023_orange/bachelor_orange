# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
# Note that the clouds.yaml from openstack is located inside ``.config/openstack/`` folder
provider "openstack" {
    cloud = "openstack"
}


module "database" {
  source = "./modules/database/"
}


module "loadbalancer" {
  source = "./modules/loadbalancer/"
  depends_on = [module.database]
}

