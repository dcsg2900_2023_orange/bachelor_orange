#!/bin/bash

# Retrieve the IP-addresses of the webservers
echo ${IP} | tr ',' '\n' >>~/ipaddresses.txt
IP_ADDRESSES=($(cat ~/ipaddresses.txt))

echo "startupscript started" >>~/status.txt

# pull the loadbalancing script from working directory
cd ~/
curl --header "PRIVATE-TOKEN: <access-token>" "<link-to-gitlab-file-api>" >> ~/

echo "Curled haproxy script" >>~/status.txt

# Run the host configuration script
~/bachelor-orange/code/scripts/hosts.sh

# Run the haproxy script
~/bachelor-orange/code/scripts/haproxy.sh

echo "Script sucessfully ran" >>~/status.txt