# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Reference to the webserver module
module "webserver" {
  source = "../webservers"
}

# Get a floating IP
resource "openstack_networking_floatingip_v2" "fip_1" {
  pool = "ntnu-internal"
}

# Create a ubuntu instance
resource "openstack_compute_instance_v2" "loadbalancer" {
  name = var.instance_name
  image_name = var.instance_image
  flavor_name = var.instance_flavor
  key_pair = var.instance_key_pair
  security_groups = var.security_groups
 
  network {
    name = var.network
  }

  user_data = "${data.template_file.script.rendered}"
  depends_on = [module.webserver]
}

output "template_contents" {
  value = data.template_file.script.rendered
}

data "template_file" "script" {
  template = "${file(var.startup_script)}"
  vars = {
    IP = join(",", [for ip in module.webserver.instance_private_ip: ip])
  }
}

# Connect Floating IP to instance
resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_1.address}"
  instance_id = "${openstack_compute_instance_v2.loadbalancer.id}"
}
