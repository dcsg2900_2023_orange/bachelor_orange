
# The file contains variable declaration and some default values. 
# This file together with main.tf and terraform.tfvars provision and instantiate a infrastructure stack
# See teraform documentation for more details of syntax/code

# @ File variables.tf
# @ Author Alexander, NTNU

# Define variables: 

variable "instance_count" {
  type = number
  default = 3
}

variable "instance_name" {
 type = string
 default = "webserver"
 description = "Name of the instance"
}

variable "instance_image" {
 type = string
 default = "Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64"
 description = "Image for the server"
}

variable "instance_flavor" {
 type = string
 default = "gx1.2c4r"
 description = "Flavor for the server"
}

variable "instance_key_pair" {
 type = string
 default = "default"
 description = "SSH key to be used to connect to the instance"
}

variable "startup_script" {
   type = string
   default = "./modules/webservers/startupscript.sh"
   description = "startup script to be run on the instance"
}

variable "network" {
  type    = string
  default = "default" # default network to be used
}

variable "security_groups" {
  type    = list(string)
  default = ["default"]  # Name of default security group
}