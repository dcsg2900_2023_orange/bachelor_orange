# main.tf

output "instance_private_ip" {
  value = openstack_compute_instance_v2.webserver.*.access_ip_v4
}