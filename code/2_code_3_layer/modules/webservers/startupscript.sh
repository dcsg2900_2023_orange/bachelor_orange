#!/bin/bash

echo "startupscript started" >>~/status.txt

# Update ubuntu packages
sudo apt update && sudo apt upgrade

# install apache
sudo apt install apache2 -y

echo "Installed apache2" >>~/status.txt

# Install php with some modules:
sudo apt install php libapache2-mod-php php-mysql -y

# Install the remaining php packages using apt
sudo apt-get install graphviz aspell ghostscript clamav php-pspell php-curl php-gd php-intl php-mysql php-xml php-xmlrpc php-ldap php-zip php-soap php-mbstring git -y
echo "Installed php" >>~/status.txt

# Change max_input_vars to 5000 for the installation capabilities, in both GUI and CLI
echo "max_input_vars = 5000" >> /etc/php/8.1/cli/php.ini
echo "max_input_vars = 5000" >> /etc/php/8.1/apache2/php.ini

# restart apache: 
sudo service apache2 restart
echo "Apache sucsessfully started" >>~/status.txt

# Move to /opt directory, and clone the Moodle Git repo
cd /opt
sudo git clone git://git.moodle.org/moodle.git

# Change to the Moodle directory and list the branches in the Moodle repository. Review the list choose the latest stable release
cd moodle
sudo git branch -a

# Track, and choose the desired git-branch
sudo git branch --track MOODLE_401_STABLE origin/MOODLE_401_STABLE
sudo git checkout MOODLE_401_STABLE

# Copy the contents of the Moodle repo to the running Apache service, and change the moderation of the moodle directory
sudo cp -R /opt/moodle /var/www/html/
sudo chmod -R 0777 /var/www/html/moodle
sudo mkdir /var/www/moodledata/

# Create the /var/www/moodledata directory and change owner and permissions
sudo chown -R www-data /var/www/moodledata
sudo chmod -R 0777 /var/www/moodledata

# Installing gluster FS

apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd
echo "GlusterFS sucsessfully installed and started" >>~/status.txt

mkdir /moodle_brick