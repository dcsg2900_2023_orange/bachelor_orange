# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Create a ubuntu instance
resource "openstack_compute_instance_v2" "database" {
  name = var.instance_name
  image_name = var.instance_image
  flavor_name = var.instance_flavor
  key_pair = var.instance_key_pair
  security_groups = var.security_groups
 
  network {
    name = var.network
  }
  
  user_data = file(var.startup_script)
}
