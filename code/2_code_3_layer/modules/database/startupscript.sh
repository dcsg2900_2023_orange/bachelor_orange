#!/bin/bash

echo "startupscript started" >>~/status.txt

# Update ubuntu packages
sudo apt update && sudo apt upgrade

# Install password generator
sudo apt install pwgen

# Install Mysql web server
sudo apt install mysql-server -y

echo "Installed mysql" >>~/status.txt

# Generate a random password only root can access
pw=$(pwgen -s 24 1)
sudo bash -c "echo $pw >> password.txt"
sudo chmod 600 password.txt



# log in to the database as root
sudo mysql -u root

sudo mysql -u root -p"" -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
sudo mysql -u root -p"" -e "CREATE USER 'admin'@'%' IDENTIFIED BY '$pw';" # Use relevant the ip-address instead of % 
sudo mysql -u root -p"" -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'admin'@'%';"
sudo sed -i 's/bind-address.*/bind-address = 0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo systemctl restart mysql

pw=""

quit



