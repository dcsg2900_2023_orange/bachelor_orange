#!/bin/bash

i=1
while read -r IP_ADDRESS; do
  echo "${IP_ADDRESS} www${i}"
  i=$((i+1))
done < ~/ipaddresses.txt >> ~/hosts

while read -r IP_ADDRESS; do
  scp ~/hosts ${IP_ADDRESS}:~ 
  ssh ${IP_ADDRESS}
  cat ~/hosts >> /etc/hosts
  i=$((i+1))
done < ~/ipaddresses.txt