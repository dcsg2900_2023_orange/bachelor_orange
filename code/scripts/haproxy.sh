#!/bin/bash

#Update ubuntu packages
sudo apt update && sudo apt upgrade -y

#Install haproxy net-tools
sudo apt-get install haproxy net-tools -y

echo "Installed haproxy" >>~/status.txt

#Configure haproxy to point to the webservers
i=1
while read -r IP_ADDRESS; do
  echo "server www${i} ${IP_ADDRESS}:80 check cookie www${i}"
  i=$((i+1))
done < ~/ipaddresses.txt > /etc/haproxy/webservers.txt

sudo sed -i 's/\$//g' "/etc/haproxy/webservers.txt"
cd /etc/haproxy/

echo "
frontend main
bind *:80
mode http
default_backend webservers

backend webservers
balance roundrobin
cookie SERVER insert indirect nocache" >> /etc/haproxy/haproxy.cfg

while read line
do
  echo "  $line" >> /etc/haproxy/haproxy.cfg
done < /etc/haproxy/webservers.txt

echo "
listen stats
bind *:1936
stats enable
stats uri /
stats hide-version
stats auth someuser:password" >> /etc/haproxy/haproxy.cfg

sudo service haproxy restart

echo "haproxy is fully installed and working" >>~/status.txt
